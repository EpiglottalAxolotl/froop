#include "buttons.h"
#include "display.h"
#include "cutscenes.h"
#include "world.h"
#include "items.h"
#include "playerchar.h"

#include <iostream>

namespace buttons {

	std::vector<sf::Texture*> buttonTextures;
	
	bool gameOpen = true;

	namespace {
		// Anonymous namespace to privatize the button system's data

		enum targetMode { noTarget=0, useItem, look };
		enum itemMode { noItems=0, inventory, ground };
		
		targetMode target_mode;
		itemMode item_mode;
		
		bool travelMode = true;
		bool interactive = true;
		
		Button end_b (6,0,SCREENWIDTH, SCREENHEIGHT-MENU_BUTTON_SIZE);
		
		Button menu_cancel_b (5,1,7*MENU_BUTTON_SIZE, SCREENHEIGHT);
				
		std::unordered_set<Button*> visibleButtons;
		
		std::vector<Button*> itemButtons;
		std::vector<Button*> dialogueButtons;
		
		const sf::IntRect TARGET_BUTTON_DEFAULT(0, 0, TILESIZE, TILESIZE);
		const sf::IntRect TARGET_BUTTON_HIGHLIGHT(0, TILESIZE, TILESIZE, TILESIZE);
		const sf::IntRect TARGET_BUTTON_PRESSED(0, TILESIZE * 2, TILESIZE, TILESIZE);
		
		const sf::IntRect INVENTORY_BUTTON_DEFAULT(TILESIZE, 0, ITEM_IMGSIZE, ITEM_IMGSIZE);
		const sf::IntRect INVENTORY_BUTTON_HIGHLIGHT(TILESIZE, ITEM_IMGSIZE, ITEM_IMGSIZE, ITEM_IMGSIZE);
		const sf::IntRect INVENTORY_BUTTON_PRESSED(TILESIZE, ITEM_IMGSIZE*2, ITEM_IMGSIZE, ITEM_IMGSIZE);
		const sf::IntRect INVENTORY_BUTTON_DISABLED(TILESIZE, ITEM_IMGSIZE*3, ITEM_IMGSIZE, ITEM_IMGSIZE);
	}
	
	int selectedItem = -1;
	
	int getSelectedItem(){return selectedItem;}
	
	int before_cancel_state = 0;
#define STATE_NO_CHAR 0
#define STATE_CHAR_MENU 1
#define STATE_INVENTORY 2
#define STATE_TARGET_ITEM 3

#define ITEMSPACE 12

	int targetX;
	int targetY;

	bool paused;
	
	void init(){

//		wait_b = Button();
		selectionsTexture.loadFromFile("img/select.png");

		end_b.effect    = [](){world::processTick();};

		menu_cancel_b.effect = [](){buttons::onCancel();};
		
		for(int i=0; i<4; ++i){
			Button* b = new Button(
				sf::IntRect(0,0,575,20),
				sf::IntRect(0,20,575,20),
				sf::IntRect(0,20*2,575,20),
				sf::IntRect(0,20*3,575,20),
				118, 481+20*i,
				"img/dialogueButtons.png"
			);
			b->effect = [i](){
				buttons::hideDialogueOptionButtons();
				cutscenes::startDialogueFromBranch(i);
			};
			dialogueButtons.push_back(b);
		}
		
		resetMenu();
	}
	
	void setTargetResult(targetMode tm){target_mode = tm;}

	bool isActive(Button* b){
		return visibleButtons.find(b) != visibleButtons.end() && ! b->isDisabled();
	}
	
	void resetMenu() {
		before_cancel_state = STATE_CHAR_MENU;
		
		setVisible(&end_b, true);
		
		setVisible(&menu_cancel_b, false);
	}
	
	int getTargetX() {
		return targetX;
	}
	
	int getTargetY() {
		return targetY;
	}
	
	void onTarget() {
		items::useItem(inventory::getItem(selectedItem)->useEffect, targetX, targetY);
		if(selectedItem > 0) inventory::removeItemAtIndex(selectedItem);
	}
	
	
	void onArrowKey(direction d){
		// navigate menus and stuff
		
		playerchar::onArrowKey(d);
	}
	
	void onNumberKey(int i){
		// probably select an item or something?
	}
	
	void onScroll(int delta) {
		selectedItem += delta;
		if (selectedItem < 0) selectedItem += inventory::count();
		if (selectedItem >= inventory::count()) selectedItem -= inventory::count();
	}
	
	void updateMousePosition(int x, int y) {
		int deltaX = x - (playerchar::getX() * TILESIZE + TILESIZE/2);
		int deltaY = y - (playerchar::getY() * TILESIZE + TILESIZE/2);
		
		targetX = playerchar::getX();
		targetY = playerchar::getY();
		
		if (deltaX * deltaX > deltaY * deltaY) {
			if (deltaX > 0) {
				++targetX;
			} else {
				--targetX;
			}
		} else {
			if (deltaY > 0) {
				++targetY;
			} else {
				--targetY;
			}
		}
	}
	
	void onItem(){
		std::cout << "onItem called" << std::endl;
		before_cancel_state = STATE_CHAR_MENU;
		selectedItem = -1;
		item_mode = itemMode::inventory;
		hideTurnMenu();
		setVisible(&menu_cancel_b, true);
		display::showInventory();
	}
	
	void onCancel(){
		if(!isActive(&menu_cancel_b)) return;
		//std::cout << "return to state " << before_cancel_state << std::endl;
		switch(before_cancel_state){
			case STATE_INVENTORY:
				onItem();
				break;
			default:
				resetMenu();
		}
	}
	
	void hideTurnMenu(){
		setVisible(&menu_cancel_b, false);
	}
	
	void resetCharactersMenu() {
		hideTurnMenu();
		setInteractive(true);
	}
	
	void setVisible(Button* b, bool visible){
		if(visible) visibleButtons.insert(b);
		else {
			b->reset();
			visibleButtons.erase(b);
		}
	}
	
	void setInteractive(bool _interactive) {
		interactive = _interactive;
		if(!interactive) {
			hideTurnMenu();
		}
	}
	
	void hideDialogueOptionButtons() {
		for(int i=0; i<dialogueButtons.size(); ++i) {
			setVisible(dialogueButtons[i], false);
		}
	}
	
	void showDialogueOptionButtons(int count) {
		for(int i=0; i<count; ++i) {
			setVisible(dialogueButtons[i], true);
		}
	}
	
	const std::unordered_set<Button*>& listButtons(){
		return visibleButtons;
	}
}


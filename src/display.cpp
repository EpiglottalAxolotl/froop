#ifndef __DISPLAY__
#define __DISPLAY__

#include <iostream>
#include <vector>
#include <random>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "core.h"
#include "display.h"
#include "buttons.h"
#include "world.h"
#include "animations.h"
#include "timekeeping.h"
#include "music.h"
#include "plants.h"
#include "items.h"
#include "playerchar.h"

using namespace std;

#define VIEW_INVENTORY 1
#define VIEW_SPELL_LIST 2
#define VIEW_GROUND_ITEMS 3
#define VIEW_CHAR_STATUS 4
#define VIEW_DEATH_SCREEN 5
#define VIEW_SPELL_DESCRIPTION 6
#define VIEW_START_MENU 256

#define CHARSPEED 1
#define CHARWIDTH 12
#define CHARHEIGHT 20
//#define TEXTWIDTH 50

#define DMGCHARW 9
#define DMGCHARH 14

#include <sstream>

namespace cutscenes {
	void setAdvanceable();
}

namespace display {
	
	int globalScale = 1;

	namespace {
		sf::Sprite tileStamp;
		sf::RenderTexture bg_texture;
		sf::Sprite background;

		sf::Sprite overlay;

		sf::Sprite plantStamp;
		sf::Texture plantTileset;
		
		sf::RenderTexture windowTexture;
		sf::Sprite windowSprite;

		sf::Texture tilesetTexture;
		int tilesetWidth;
		int tilesetHeight;

		sf::Texture oobTexture;
		sf::Sprite outOfBounds;

		// these are in tiles, not pixels
		int offsetX = 0;
		int offsetY = 0;
		
		int _view;
		
		int mouseX, mouseY;
				
		vector<shared_ptr<animations::AnimationInstance>> foregroundAnimations;
		vector<shared_ptr<animations::AnimationInstance>> backgroundAnimations;
		sf::Sprite charSprite;
		sf::Texture fontTextures[4];
				
		sf::Texture dmgFontTexture;
		
		bool isDialogue = false;
		int dialogueStartTime;
		int dialogueTickCount;
		string dialogueText;
		string dialogueName;
		int dialogueSoundId;
		int dialogueSpriteId;
		int dialogueResponseCount;
		shared_ptr<std::vector<std::string> > dialogueResponseOptions;

		bool isSceneImage;
		sf::Texture sceneImageTexture;
		sf::Sprite sceneImageSprite;
		
		sf::Texture textboxTexture;
		sf::Sprite dialogueTextboxSprite;
		
		sf::Texture statusPageTexture;
		sf::Sprite statusTextboxSprite;
		
		sf::Texture talkspriteTexture;
		sf::Sprite dialogueFaceSprite;
		
		sf::Texture cursorTexture;
		sf::Sprite cursorSprite;
		
		sf::Texture selectTexture;
		sf::Sprite selectedItemSprite;
		sf::Sprite mouseHoverSprite;
	}
	
	void loadBackground() {
		bg_texture.create(GARDEN_SIZE*TILESIZE, GARDEN_SIZE*TILESIZE);

		for(int x=0; x<GARDEN_SIZE; ++x) {
			for(int y=0; y<GARDEN_SIZE; ++y) {
				int soilTexture = world::getTerrain(x, y);
				//if(soilTexture == 0) {
				//	if(plot.hydration > 0) soilTexture = 1;
				//	else if(plot.hydration < 0) soilTexture = 2;
				//}
				
				// TODO this should take into consideration tileset width if the number of textures gets large
				tileStamp.setTextureRect(sf::IntRect(soilTexture*TILESIZE, 0, TILESIZE, TILESIZE ));
				tileStamp.setPosition(x*TILESIZE, y*TILESIZE);
				bg_texture.draw(tileStamp);
			}
		}
	}
	
	void init(){		
		for(int i=0; i<4; ++i) fontTextures[i].loadFromFile("img/font/"+to_string(i)+".png");


		windowTexture.create(SCREENWIDTH+128,SCREENHEIGHT+64);
		
		charSprite.setTexture(fontTextures[0]);
		
		dmgFontTexture.loadFromFile("img/font/dmg.png");
		
		textboxTexture.loadFromFile("img/textbox.png");
		dialogueTextboxSprite.setTexture(textboxTexture);
		dialogueTextboxSprite.setPosition(48,409);
		
		statusPageTexture.loadFromFile("img/statuspage.png");
		statusTextboxSprite.setTexture(statusPageTexture);
		statusTextboxSprite.setPosition(48,48);
		
		talkspriteTexture.loadFromFile("img/talksprites.png");
		dialogueFaceSprite.setTexture(talkspriteTexture);
		dialogueFaceSprite.setPosition(55,416);
		
		//sceneImageTexture.loadFromFile("img/scene/0.png");
		sceneImageSprite.setTexture(sceneImageTexture);
		sceneImageSprite.setPosition(217, 242);
		//sceneImageSprite.setTextureRect(sf::IntRect(217, 242, 320, 128));
		
		bg_texture.create(GARDEN_SIZE*TILESIZE, GARDEN_SIZE*TILESIZE);

		tilesetTexture.loadFromFile("img/tiles.png");
		tilesetWidth = tilesetTexture.getSize().x / TILESIZE;
		tilesetHeight = tilesetTexture.getSize().y / TILESIZE;

		tileStamp.setTexture(tilesetTexture);
		
		cursorTexture.loadFromFile("img/cursor.png");
		cursorSprite.setTexture(cursorTexture);
		
		plantTileset.loadFromFile("img/plants.png");
		plantStamp.setTexture(plantTileset);
		
		loadBackground();
		
		bg_texture.display();
		background = sf::Sprite(bg_texture.getTexture());
		
		oobTexture.loadFromFile("img/bgoob.png");
		oobTexture.setRepeated(true);
		outOfBounds.setTexture(oobTexture);
		
		selectTexture.loadFromFile("img/select.png");
		selectedItemSprite.setTexture(selectTexture);
		selectedItemSprite.setTextureRect(sf::IntRect(0,0,ITEM_IMGSIZE,ITEM_IMGSIZE));
		mouseHoverSprite.setTexture(selectTexture);
		mouseHoverSprite.setTextureRect(sf::IntRect(0,0,ITEM_IMGSIZE,ITEM_IMGSIZE));
	}
	
	void setGlobalScale(int scale) {
		globalScale = scale;
		windowSprite.setScale(scale,scale);
	}
	
	int getGlobalScale() {
		return globalScale;
	}
	
	bool isClearView(){return _view==0;}
	bool isStartMenu(){return _view>=VIEW_START_MENU;}
	void clearView(){_view = 0;}
	void showInventory(){_view = VIEW_INVENTORY;}
	void showSpellList(){_view = VIEW_SPELL_LIST;}
	void showSpellDescription(){_view = VIEW_SPELL_DESCRIPTION;}
	void showGroundItems(){_view = VIEW_GROUND_ITEMS;}
	void showCharacterStatus(){
		_view = VIEW_CHAR_STATUS;
	}
	void showDeathScreen(){
		_view = VIEW_DEATH_SCREEN;
		dialogueStartTime = timekeeping::currentTick();
	}

	void centerCoords(int x, int y) {
		offsetX = x - (SCREENWIDTH/2/TILESIZE);
		offsetY = y - (SCREENHEIGHT/2/TILESIZE);
	}
	
	void center(int x, int y){
		centerCoords(x, y);
	}
	
	std::list<std::pair<std::pair<int, int>, int>> tileUpdateQueue;
	
	void scheduleSetTile(int x, int y, int t){
		tileUpdateQueue.push_back(std::make_pair(std::make_pair(x,y),t));
	}
	
	void setTile(int x, int y, int t){
		tileStamp.setTextureRect(sf::IntRect((t%tilesetWidth)*TILESIZE, (t/tilesetWidth)*TILESIZE, TILESIZE, TILESIZE ));
		tileStamp.setPosition(x*TILESIZE, y*TILESIZE);
		bg_texture.draw(tileStamp);
	}

	void updateMouse(int x, int y){
		mouseX = x / globalScale;
		mouseY = y / globalScale;
	}
	
	void addAnimation(int x, int y, const shared_ptr<animations::animation>& a){
		foregroundAnimations.push_back(shared_ptr<animations::AnimationInstance> { new animations::AnimationInstance(x, y, false, a) });
	}
	
	void addAnimationAtTile(int x, int y, shared_ptr<animations::animation>& a){
		backgroundAnimations.push_back(shared_ptr<animations::AnimationInstance> { new animations::AnimationInstance(x, y, true, a) });
	}
	
	int getEscapedWidth(std::string text){
		int width = 0;
		int line = 0;
		for(int i=0; i<text.length(); ++i) {
			if(text[i]=='%') line-=2;
			else if(text[i] == '\n') {
				if(line > width) width = line;
				line = 0;
			}
			else ++line;
		}
		if(line > width) width = line;
		return width;
	}
	
	int getEscapedLength(std::string text){
		int line = 0;
		for(int i=0; i<text.length(); ++i) {
			if(text[i]=='%') line-=2;
			else ++line;
		}
		return line;
	}
	
	std::string padInt(int i, int width){
		std::string output = to_string(i);
		while(output.length() < width) output = " " + output;
		return output;
	}

	void skipDialogue() {
		dialogueStartTime = 0;
	}
	
	void showDialogue(const std::string& text, const std::string& name, int talkspriteid, int voiceid) {
		isDialogue = true;
		dialogueText = text;
		dialogueName = name;
		dialogueSpriteId = talkspriteid;
		dialogueSoundId = voiceid;
		dialogueStartTime = timekeeping::currentTick();
		dialogueTickCount = 0;
		dialogueResponseCount = 0;
	}
	
	void showDialogueOptions(int count, const std::shared_ptr<std::vector<string> >& options) {
		dialogueResponseCount = count;
		dialogueResponseOptions = options;
	}
	
	void hideDialogue(){
		isDialogue = false;
		isSceneImage = false;
	}
	
	void showSceneImage(int index) {
		if(index < 0) isSceneImage = false;
		else {
			isSceneImage = true;
			sceneImageTexture.loadFromFile("img/scene/"+std::to_string(index)+".png");
			sceneImageSprite.setTexture(sceneImageTexture);
		}
	}

	struct damageText {
		int amt;
		int x;
		int y;
		int endTick;
	};
	
	std::vector<damageText> damageTexts;
	
	void showDamageText(int amt, int x, int y){
		damageText dt;
		dt.amt = amt;
		dt.x = x;
		dt.y = y;
		dt.endTick = timekeeping::currentTick() + 15;
		damageTexts.push_back(dt);
	}
	
	void spliceStatText(std::string& text, int startIndex) {
		int removeLength;
		std::string newText;
		
		switch(text[startIndex]) {
			/*case 'w': { // weapon-based attack
				weapon* w = pc->getEquippedWeapon();
				newText = "%R" + std::to_string(w->baseDamage * pc->getDamageDealt(w->dtype) / 100) + "%S" + (char)(w->dtype + '0') + "%c";
				removeLength = 1;
				break;
			}*/
		}
		
		text.erase(startIndex, removeLength);
		text.insert(startIndex, newText);
	}

	void drawText(int originX, int originY, std::string text, int maxWidth = 50){
		//(timekeeping::currentTick() - dialogueStart)/CHARSPEED
		// Draw background
		
		int tileset = 0;
		
		int readPos=0;
		int writePos=0;
		while(readPos < text.length()) {
			if(text[readPos] == '%'){
				++readPos;
				switch(text[readPos]){
					case 'R': tileset=1; break; // red
					case 'B': tileset=2; break; // blue
					case 'S': tileset=3; break; // special characters
					case 'c': tileset=0; break; // clear color
					case 'n': originY += CHARHEIGHT; writePos = 0; break; // newline
					case '%': { // character stat escape sequences
						spliceStatText(text, readPos+1);
					}
				}
			} else if(text[readPos] == '\n'){
				originY += CHARHEIGHT;
				writePos = 0;
			} else {

				charSprite.setTexture(fontTextures[tileset]);


				charSprite.setTextureRect(sf::IntRect((text[readPos]-' ') % 12 * CHARWIDTH, (text[readPos]-' ') / 12 * CHARHEIGHT, CHARWIDTH, CHARHEIGHT)); // 12 x 8 ASCII
				
				int x = originX + writePos % maxWidth * CHARWIDTH;
				int y = originY + writePos / maxWidth * CHARHEIGHT;				
				charSprite.setPosition(x,y);
				
				windowTexture.draw(charSprite);
				++writePos;
			}
			++readPos;
		}
	}
	
	void narrateText(int originX, int originY, std::string text, int voiceId, int maxWidth = 50){
				
		//(timekeeping::currentTick() - dialogueStart)/CHARSPEED
		// Draw background
		
		int maxLength = (timekeeping::currentTick() - dialogueStartTime)/CHARSPEED;
		
		int tileset = 0;
		bool wave = false;
		int vibrate = 0;
		
		int readPos=0;
		int writePos=0;
		int writeTime=0;
		while(writeTime < maxLength && readPos < text.length()) {
			if(text[readPos] == '%'){
				++readPos;
				switch(text[readPos]){
					case 'R': tileset=1; break; // red
					case 'B': tileset=2; break; // blue
					case 'S': tileset=3; break; // special characters
					case 'c': tileset=0; break; // clear color
					case 'W': wave = true; break;
					case 'V': vibrate += 1; break; // vibrate
					case 't': wave=false; vibrate=0; break; // clear texture
					case 'P': ++writeTime; break; // pause
					case 'F': --writeTime; break; // fast
					case 'n': originY += CHARHEIGHT; writePos = 0; break; // newline
				}
			} else if(text[readPos] == '\n'){
				originY += CHARHEIGHT;
				writePos = 0;
			} else {

				charSprite.setTexture(fontTextures[tileset]);


				charSprite.setTextureRect(sf::IntRect((text[readPos]-' ') % 12 * CHARWIDTH, (text[readPos]-' ') / 12 * CHARHEIGHT, CHARWIDTH, CHARHEIGHT)); // 12 x 8 ASCII
				
				int x = originX + writePos % maxWidth * CHARWIDTH;
				int y = originY + writePos / maxWidth * CHARHEIGHT;
				if(vibrate) {
					x += rand() % (2*vibrate+1) - (vibrate+1);
					y += rand() % (2*vibrate+1) - (vibrate+1);
				} else if (wave) {
					int wavePos = (timekeeping::currentTick()+3*writePos) % 21 - 5;
					if(wavePos > 5) wavePos = 10 - wavePos;
					y += wavePos;
				}
				charSprite.setPosition(x,y);
				
				windowTexture.draw(charSprite);
				++writePos;
				++writeTime;
			}
			++readPos;
		}
				
		if(writeTime == maxLength && maxLength > dialogueTickCount) {
			// Hypothetical possibilities: randomize more for vibrate. do something higher and weirder for wave, etc; also %v
			double pitch = 1;
			
			if(wave) {
				pitch += .01 * (writeTime % 21);
			}
			
			if(tileset == 1 || tileset == 2) {
				pitch += .1;
			}
			
			if(vibrate) {
				pitch += (rand() % 64) / 128.0 - (32/128.0);
			} else if(voiceId >= -1) {
				pitch += (rand() % 16) / 128.0 - (8/128.0);
			}
			
			/* 0.875 + (rand() % 16) / 64.0 */
			
			if(voiceId < -1) voiceId = -1;
			
			dialogueTickCount = maxLength;
			if(text[readPos-1] >= 'A' && text[readPos-1] <= 'z') music::playSound("voice/"+std::to_string(voiceId), pitch);
		} else if(readPos == text.length() && _view != VIEW_DEATH_SCREEN) {
			cutscenes::setAdvanceable();
		}
	}
	
	void drawItemInfoPanel(item* i){
		i->getSprite()->setPosition(8*ITEM_IMGSIZE, SCREENHEIGHT);
		windowTexture.draw(*(i->getSprite()));
		drawText(9*ITEM_IMGSIZE, SCREENHEIGHT, "%B"+i->getName());
		drawText(9*ITEM_IMGSIZE, SCREENHEIGHT+CHARHEIGHT, i->getFlavor(), (SCREENWIDTH+128 - 9*ITEM_IMGSIZE)/CHARWIDTH);
	}
	
	
	bool hasDialogue() {
		return isDialogue;
	}
	
	void drawWindow(sf::RenderWindow& window){
		
		while(!tileUpdateQueue.empty()){
			pair<pair<int,int>,int> update = tileUpdateQueue.front();
			tileUpdateQueue.pop_front();
			setTile(update.first.first, update.first.second, update.second); //, world::getAllTileImages(), world::width, world::height); needed for interpolation
		}
		bg_texture.display();
		
		window.clear(sf::Color(103,103,103));
		windowTexture.clear(sf::Color(103,103,103));
		
		outOfBounds.setTextureRect(sf::IntRect(offsetX*TILESIZE/2,offsetY*TILESIZE/2,SCREENWIDTH, SCREENHEIGHT));
		windowTexture.draw(outOfBounds);
				
		string tooltipText = "";
		
		background.setPosition(-offsetX*TILESIZE, -offsetY*TILESIZE);
		background.setTextureRect(sf::IntRect(0,0, std::min((int)bg_texture.getSize().x, SCREENWIDTH+offsetX*TILESIZE), std::min((int)bg_texture.getSize().y, SCREENHEIGHT+offsetY*TILESIZE)));
		windowTexture.draw(background); // need to cast to int because they were uint

		int playerIndex = world::toIndex(playerchar::getX(), playerchar::getY());

		auto itr = world::getAllPlants().begin();
		while(itr != world::getAllPlants().end() && itr->first <= playerIndex) {
			PlantType type = plantTypes::get(itr->second.plantId);
			plantStamp.setTextureRect(sf::IntRect((itr->second.growthStage/type.stagesPerFrame)*TILESIZE, itr->second.plantId*TILESIZE, TILESIZE, TILESIZE));
			plantStamp.setPosition(itr->first%GARDEN_SIZE*TILESIZE, itr->first/GARDEN_SIZE*TILESIZE - TILESIZE/2);
			windowTexture.draw(plantStamp);
			
			++itr;
		}

		playerchar::getSprite().setPosition((playerchar::getX() - offsetX) * TILESIZE, (playerchar::getY() - offsetY) * TILESIZE - 6);
		windowTexture.draw(playerchar::getSprite());
		
		while(itr != world::getAllPlants().end()) {
			PlantType type = plantTypes::get(itr->second.plantId);
			plantStamp.setTextureRect(sf::IntRect((itr->second.growthStage/type.stagesPerFrame)*TILESIZE, itr->second.plantId*TILESIZE, TILESIZE, TILESIZE));
			plantStamp.setPosition(itr->first%GARDEN_SIZE*TILESIZE, itr->first/GARDEN_SIZE*TILESIZE - TILESIZE/2);
			windowTexture.draw(plantStamp);
			
			++itr;
		}

		auto item_itr = inventory::enumerateItems();
		int i = 0;
		while(item_itr != inventory::itemsEnd()) {
			item* iow = items::getItem(item_itr->first);
			sf::Sprite* s = iow->getSprite();
			s->setPosition((i%4)*ITEM_IMGSIZE + SCREENWIDTH, (i/4)*ITEM_IMGSIZE);
			windowTexture.draw(*s);
			if(item_itr->second > 1) drawText((i%4)*ITEM_IMGSIZE + SCREENWIDTH, (i/4 + 1)*ITEM_IMGSIZE - CHARHEIGHT, std::to_string(item_itr->second));
			
			if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
				tooltipText = iow->getName();
			}
		
			++item_itr;
			++i;
		}
		selectedItemSprite.setPosition((buttons::getSelectedItem()%4)*ITEM_IMGSIZE + SCREENWIDTH, (buttons::getSelectedItem()/4)*ITEM_IMGSIZE);
		windowTexture.draw(selectedItemSprite);

		mouseHoverSprite.setPosition(buttons::getTargetX()*ITEM_IMGSIZE, buttons::getTargetY()*ITEM_IMGSIZE);
		windowTexture.draw(mouseHoverSprite);
		
		if(buttons::getSelectedItem() >= 0 && buttons::getSelectedItem() < SELECT_EQUIPMENT){
				drawItemInfoPanel(items::getItem(buttons::getSelectedItem())
			);
		}

		if(_view == VIEW_INVENTORY){
			
		} else if(_view == VIEW_CHAR_STATUS) {
			windowTexture.draw(statusTextboxSprite);
			std::stringstream infopile; // infopile rhymes with monopoly
			
			infopile << "\n";

			drawText(55,55,infopile.str());
		}
		
		
		if(isDialogue) windowTexture.draw(dialogueTextboxSprite);
			
		const unordered_set<Button*>& visibleButtons = buttons::listButtons();
		for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr) windowTexture.draw(*(*itr)->getSprite());
		
		
		
		for(auto itr=backgroundAnimations.begin(); itr!=backgroundAnimations.end();){
			sf::Sprite* s = (*itr)->update();
			if(s) {
				if((*itr)->atTile) s->setPosition(((*itr)->offsetX - offsetX) * TILESIZE, ((*itr)->offsetY - offsetY) * TILESIZE);
				windowTexture.draw(*s);
				++itr;
			}
			else { 
				itr = backgroundAnimations.erase(itr); 
			}
		}
		
		for(auto itr=damageTexts.begin(); itr!=damageTexts.end();){
			int now = timekeeping::currentTick();
			if(now > itr->endTick) {
				itr = damageTexts.erase(itr); 
			}
			else {
				std::string text = std::to_string(itr->amt);
				
				int baseX = (itr->x - offsetX)*TILESIZE + (TILESIZE - getEscapedWidth(text)*DMGCHARW)/2;
				charSprite.setTexture(dmgFontTexture);
				
				for(int i = 0; i < text.length(); ++i){
					if(text[i] == '-') {
						charSprite.setTextureRect(sf::IntRect(DMGCHARW*10, DMGCHARH, DMGCHARW, DMGCHARH));
					} else if (text[i] == '0' && text.length() == 1) {
						charSprite.setTextureRect(sf::IntRect(DMGCHARW*10, 0, DMGCHARW, DMGCHARH));
					} else {
						charSprite.setTextureRect(sf::IntRect((text[i]-'0')*DMGCHARW, DMGCHARH*(itr->amt < 0), DMGCHARW, DMGCHARH));
					}
					charSprite.setPosition(baseX + i*DMGCHARW, (itr->y-offsetY)*TILESIZE-30 + (itr->endTick- now)*3);
					charSprite.setPosition(baseX + i*DMGCHARW, (itr->y-offsetY)*TILESIZE-30 + (itr->endTick- now)*3);
					windowTexture.draw(charSprite);
				}
				++itr;
			}
		}
		
		if(isDialogue) {
			int textboxStyleOffset = 0;
			if(dialogueName.length() == 0) textboxStyleOffset = 318;
			else if(dialogueSpriteId < 0) textboxStyleOffset = 159;
			// TODO don't hardcode some of these numbers
			dialogueTextboxSprite.setTextureRect(sf::IntRect(0, textboxStyleOffset, 657, 159));

			if(dialogueSpriteId >= 0){
				int talkspritesWidth = talkspriteTexture.getSize().x / ITEM_IMGSIZE;
				dialogueFaceSprite.setTextureRect(sf::IntRect((dialogueSpriteId%talkspritesWidth)*ITEM_IMGSIZE, (dialogueSpriteId/talkspritesWidth)*ITEM_IMGSIZE, ITEM_IMGSIZE,ITEM_IMGSIZE));
				windowTexture.draw(dialogueFaceSprite);
				drawText(124,415, dialogueName);
				
				narrateText(124,442, dialogueText, dialogueSoundId, 48);
			} else if(dialogueName.length() > 0) {
				drawText(55,415, dialogueName);
				narrateText(55,442, dialogueText, dialogueSoundId, 53);
			} else {
				narrateText(55,415, dialogueText, dialogueSoundId, 53);
			}
		
			for(int i=0; i<dialogueResponseCount; ++i) {
				drawText(123, 482 + 20*i, (*dialogueResponseOptions)[i]);
			}
			
			if(isSceneImage) {
				windowTexture.draw(sceneImageSprite);
			}
		}
		
		if(tooltipText.length() > 0){
			/*sf::Text tooltip(tooltipText, font, 20);
			//tooltip.setOutlineColor(sf::Color::Black);
			tooltip.setFillColor(sf::Color::Black);
			tooltip.setPosition(mouseX - (int)(tooltip.getLocalBounds().width), mouseY - (int)(tooltip.getLocalBounds().height));
			windowTexture.draw(tooltip); // TODO background for it*/
			drawText(mouseX - getEscapedWidth(tooltipText)*CHARWIDTH, mouseY - CHARHEIGHT, tooltipText);
		
		}
		
		cursorSprite.setPosition(mouseX, mouseY);
		windowTexture.draw(cursorSprite);
		
		windowTexture.display();
		windowSprite.setTexture(windowTexture.getTexture());
		
		window.draw(windowSprite);
		window.display();
	}
}

#endif

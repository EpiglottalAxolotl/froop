#ifndef __WORLD__
#define __WORLD__

#include <map>

#include "core.h"
#include "plants.h"

#define MASK_SOIL    0x0007 // 0x1 & 0x2 & 0x4
#define MASK_HARVEST 0x0038 // 0x08 & 0x10 & 0x20
#define MASK_GROWTH  0x07C0 // 0x040 & 0x080 & 0x100 & 0x200 & 0x400
#define MASK_ID      0xF800 // 0x0800 & 0x1000 & 0x2000 & 0x4000 & 0x8000

#define OFFSET_HARVEST 3
#define OFFSET_GROWTH 6
#define OFFSET_ID 11

namespace world {
	void loadSave();
  
	void processTick();
	const Plant& getPlot(int x, int y);
	int getTerrain(int x, int y);
	
	void onLook(int, int);
  
	bool blocked(int, int);
	
	void plantCrop(int, int, int);
	
	const std::map<int, Plant>& getAllPlants();
	
	int toIndex(int x, int y);
	
	void harvestCrop(int, int);
}

#endif

#include "plants.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

namespace plantTypes {
	
	std::vector<PlantType> types;

	void init() {
		ifstream typesFile("misc/plants.asdf");
		string line;
		stringstream lineStream;
		
		int id = 0;
		while(getline(typesFile, line)) {
			lineStream.str(line);
			lineStream.clear();
			
			types.push_back(PlantType());
			types[id].id = id;
			lineStream >> types[id].defaultGrowthRate >> types[id].defaultYield >> types[id].lightYield >> types[id].growthTime >> types[id].stagesPerFrame;
			
			++id;
		}
	}
	
	const PlantType get(int i) {
		return types[i];
	}
}
#ifndef CORE
#define CORE

#define TILESIZE 24

#define GARDEN_SIZE 32

enum direction { _left, _up, _right, _down };

#define NUM_DAMAGE_TYPES 6
enum damage_type { physical, holy, fire, frost, poison, corrupt };

enum range_type { self, melee, radius, linear, long_melee, radius_line, custom };

#endif

#include "buttons.h"

namespace buttons {
	sf::Texture selectionsTexture;
}

Button::Button(sf::IntRect defaultSlice, sf::IntRect highlightSlice, sf::IntRect pressedSlice, sf::IntRect disabledSlice, int x, int y, const std::string& textureFilename) : _defaultSlice(defaultSlice), _highlightSlice(highlightSlice), _pressedSlice(pressedSlice), _disabledSlice(disabledSlice), _x(x), _y(y) {
	
	static std::map<std::string, sf::Texture*> textureCache;
	
	if(textureCache.find(textureFilename) == textureCache.end()){
		sf::Texture* tex = new sf::Texture;
		tex->loadFromFile(textureFilename);
		textureCache[textureFilename] = tex;
	}
	
	sf::Texture* tex = textureCache[textureFilename];
	sprite = new sf::Sprite(*tex);
	
	sprite->setPosition(x,y);
	sprite->setTextureRect(defaultSlice);
	
	rect = sf::IntRect(x, y, defaultSlice.width, defaultSlice.height);
	
	_disabled = false;
	
}

Button::Button(int tilesetX, int tilesetY, int screenX, int screenY) : Button ( 
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,tilesetY*4*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,(tilesetY*4+1)*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,(tilesetY*4+2)*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,(tilesetY*4+3)*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		screenX, screenY,
		"img/buttons.png"
	) {}

sf::Sprite* Button::getSprite() {
	//sprite->setTextureRect(_disabled ? _disabledSlice : _highlight ? _highlightSlice : _defaultSlice);
	return sprite;
}

void Button::highlight(){
	if(!_disabled) sprite->setTextureRect(_highlightSlice);
}
void Button::depress(){
	if(!_disabled) sprite->setTextureRect(_pressedSlice);
}
void Button::reset(){
	sprite->setTextureRect(_disabled ? _disabledSlice : _defaultSlice);
}
void Button::disable(bool disable){
	//if(disable) sprite->setTextureRect(_disabledSlice);
	_disabled = disable;
	reset();
}

#include "world.h"
#include "animations.h"
#include "display.h"
#include "plants.h"
#include "music.h"
#include "cutscenes.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <unordered_set>

using namespace std;

// forward declarations
namespace inventory {
	void takeItems(int, int);
}

typedef map<int, Plant> Garden;

namespace world {
	
	std::unordered_set<int> globalFlags;
	int terrains[GARDEN_SIZE][GARDEN_SIZE];
	Garden plants;
	
	int toIndex(int x, int y) {
		return y*GARDEN_SIZE + x;
	}
	
	bool typeIfInBounds(int x, int y) {
		if (x < 0 || y < 0 || x >= GARDEN_SIZE || y >= GARDEN_SIZE) return NO_PLANT;
		auto itr = plants.find(toIndex(x,y));
		if (itr == plants.end()) return NO_PLANT;
		return itr->second.plantId;
	}
	
	int hydrate(int type, int delta) {
		// TODO
		return type;
	}
	
	int getTerrain(int x, int y) {
		return terrains[x][y];
	}
	
	int getGrowthRateOnSoil(PlantType type, int terrain) {
		return type.defaultGrowthRate;
	}
	
	void setTerrain(int x, int y, int t) {
		if(terrains[x][y] != t) display::scheduleSetTile(x, y, t);
	}
	
	void processTick() {
		
		Garden tomorrow;
		
		char hydrations[GARDEN_SIZE][GARDEN_SIZE];
		char illuminations[GARDEN_SIZE][GARDEN_SIZE];
		
		// Calculate soil conditions: hydration, illumination, etc
		for(int x=0; x<GARDEN_SIZE; ++x) {
			for(int y=0; y<GARDEN_SIZE; ++y) {
				int index = toIndex(x,y);
				
				hydrations[x][y] = 0;
				
				if(terrains[x][y] == TYPE_POOL) {
					for(int i=-1; i<=1; ++i) if(i+x >= 0 && i+x < GARDEN_SIZE) {
						for(int j=-1; j<=1; ++j) if(j+y >= 0 && j+y < GARDEN_SIZE) {
							hydrations[x][y] += 1;
						}
					}
				}
				
				if(typeIfInBounds(x,y) == PLANT_SUNFLOWER && plants[index].growthStage >= 3) {
					for(int i=1; i<=2; ++i) if(i+x >= 0 && i+x < GARDEN_SIZE) {
						for(int j=-1; j<=1; ++j) if(j+y >= 0 && j+y < GARDEN_SIZE) {
							illuminations[x+i][y+j] += 1;
							hydrations[x+i][y+j] -= 1;
						}
					}
				} else if(typeIfInBounds(x,y) == PLANT_MOONFLOWER && plants[index].growthStage >= 3) {
					if(x >= 1) {
						for(int j=-1; j<=1; ++j) if(j+y >= 0 && j+y < GARDEN_SIZE) {
							illuminations[x-1][y+j] += 1;
						}
					}
				} 
				
			}
		}
		
		for(int x=0; x<GARDEN_SIZE; ++x) {
			for(int y=0; y<GARDEN_SIZE; ++y) {
				if(hydrations[x][y] != 0) {
					setTerrain(x, y, hydrate(terrains[x][y], hydrations[x][y]));
				}
			}
		}
		
		// Plants grow
		for(auto itr = plants.begin(); itr != plants.end(); ++itr) {
			
			PlantType type = plantTypes::get(itr->second.plantId);
			int index = itr->first;
			int x = index % GARDEN_SIZE;
			int y = index / GARDEN_SIZE;
			
			if (itr->second.growthStage >= type.growthTime) {
				//check for propagation
				
				
				if (itr->second.plantId == PLANT_FROOP
				&& typeIfInBounds(x,y+1) == NO_PLANT
				&& typeIfInBounds(x-1,y+1) == PLANT_FROOP
				&& typeIfInBounds(x+1,y+1) == PLANT_FROOP
				&& typeIfInBounds(x,y+2) == PLANT_FROOP
				&& plants[toIndex(x-1,y+1)].growthStage >= type.growthTime
				&& plants[toIndex(x+1,y+1)].growthStage >= type.growthTime
				&& plants[toIndex(x,y+2)].growthStage >= type.growthTime) {
					tomorrow[toIndex(x,y+1)] = Plant();
					tomorrow[toIndex(x,y+1)].plantId = PLANT_SPECIAL_FROOP;
					tomorrow[toIndex(x,y+1)].growthStage = 0;
				}
			}
			
			int growthDelta = getGrowthRateOnSoil(type, terrains[x][y]);
			if(growthDelta < 0) { // plant cannot grow on this soil
				continue; // in the future, could add some sort of dead plant here
			}
			
			tomorrow[index] = itr->second;
			
			if(growthDelta + itr->second.growthStage >= type.growthTime) {
				tomorrow[index].growthStage = type.growthTime;
				tomorrow[index].harvestReady = plants[index].harvestAccumulated / 100;
			} else {
				tomorrow[index].growthStage = plants[index].growthStage + growthDelta;
				tomorrow[index].harvestAccumulated = plants[index].harvestAccumulated + (illuminations[x][y] > 0 ? type.lightYield : type.defaultYield);
			}
		}
		
		plants = tomorrow;
		
		//display::updateBackground(); // in case any terrains changed due to hydration levels.
		// however: should change this to a queued-update system
	}
	
	const Garden& getAllPlants() {
		return plants;
	}
	
	void onLook(int x, int y) {
		// TODO
	}
	
	bool hasGlobalFlag(int flag) {
		return globalFlags.find(flag) != globalFlags.end();
	}
	
	void addGlobalFlag(int flag) {
		globalFlags.insert(flag);
	}
	
	void removeGlobalFlag(int flag) {
		globalFlags.erase(flag);
	}
	
	void plantCrop(int plantType, int x, int y) {
		plants[toIndex(x,y)] = Plant();
		plants[toIndex(x,y)].plantId = plantType;
		plants[toIndex(x,y)].growthStage = 0;
	}
	
	void loadSave() {
		
	}
	
	void loadSaveOld() {
		// DOESN'T WORK
		ifstream gardenStream("save/garden", std::ios::binary);
		if(!gardenStream.is_open()) gardenStream = ifstream("misc/default", std::ios::binary);
		
		uint16_t raw[GARDEN_SIZE*GARDEN_SIZE];
		gardenStream.read((char*)raw, 2*GARDEN_SIZE*GARDEN_SIZE);
		
		int i=0;
		for(int x=0; x<GARDEN_SIZE; ++x) {
			for(int y=0; y<GARDEN_SIZE; ++y) {
				//plants[toIndex(x,y)].soilType     = (raw[i] & MASK_SOIL);
				plants[toIndex(x,y)].harvestReady = (raw[i] & MASK_HARVEST) >> OFFSET_HARVEST;
				plants[toIndex(x,y)].growthStage  = (raw[i] & MASK_GROWTH)  >> OFFSET_GROWTH;
				plants[toIndex(x,y)].plantId      = (raw[i] & MASK_ID)      >> OFFSET_ID;
				++i;
			}
		}

		// also load inventory

	}
	
	void harvestCrop(int x, int y) {
		if(plants.find(toIndex(x,y)) == plants.end()) return;
		if(plants[toIndex(x,y)].harvestReady) {
			inventory::takeItems(1, plants[toIndex(x,y)].harvestReady);
			plants.erase(toIndex(x,y));
		}
	}

}



#ifndef __BUTTONS__
#define __BUTTONS__

#include <utility>
#include <functional>
#include <vector>
#include <unordered_set>

#include "core.h"
//#include "characters.h"
//#include "weapons.h"

#include <SFML/Graphics.hpp>

#define MENU_BUTTON_SIZE 64
#define SELECT_WEAPON 0x100
#define SELECT_EQUIPMENT 0x80

#define MAX_SETTINGS_MENU 4

class Button {
	public:
		Button(sf::IntRect, sf::IntRect, sf::IntRect, sf::IntRect, int, int, const std::string&);
		Button(int, int, int, int);
		Button(int, int, int);
	
		sf::IntRect rect;
		
		void highlight();
		void depress();
		void reset();		
		
		void disable(bool);
		
		void trigger();
				
		sf::Sprite* getSprite();
		
		bool isDisabled() const {return _disabled;};
		
		std::function<void()> effect = [](){};
		
	private:
		sf::Sprite* sprite;
		
		//void (*effect)();
		bool _disabled;
		bool _pressed;
		
		int _x;
		int _y;
		
		sf::IntRect _defaultSlice;
		sf::IntRect _highlightSlice;
		sf::IntRect _disabledSlice;
		sf::IntRect _pressedSlice;
		
		//static std::map<std::string, sf::Texture*> textureCache;
};


namespace buttons {
	extern bool gameOpen;

	const std::unordered_set<Button*>& listButtons();
	
	void setInteractive(bool);
	
	bool isPaused();
	void hideTurnMenu();
	void target(int, int);
	void allowTargets(const std::vector<std::pair<int,int> >&);
	void showDialogueOptionButtons(int);
	void hideDialogueOptionButtons();
	void selectItem(int);
	void resetMenu();
	
	void setVisible(Button*, bool);
	void setMenuVisible(Button*, bool);
	void onPause();
	void onUnpause();
	
	void onAdjustMusic(int);
	
	void onStatus();
	void onMove();
	void onAttack();
	void onSpirit();
	void onItem();
	void onWait();
	
	int getTargetX();
	int getTargetY();
	void onTarget();
	
	void onCancel();
	void onEndTurn();
	void onItemTake();
	void onItemGive();
	void onItemDrop();
	void onItemGround();
	void onItemRemove();
		
	void onNumberKey(int);
	void onArrowKey(direction);
	void onItemHotkey();
	void onSpiritHotkey();
	void onSpacebar();
	void toNextCharacter(bool);
	void onScroll(int);
	void updateMousePosition(int, int);
	
	void init();
	void showTitleScreen();
	
	int getSelectedCharacter();
	int getSelectedItem();

	void addPlayerCharacter();
	
	void showDeathScreen();
	
	extern sf::Texture selectionsTexture;
	extern sf::Texture selectionsTexture;
}

#endif

// The targetting buttons will call target_result(x,y) when clicked.
// Which means target_result should do... something.
// If the thing being targetted is a weapon, target_result will just be the weapon's damage function.

//target_result takes as input two integers (coordinates) and 

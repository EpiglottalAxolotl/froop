#include <iostream>
#include <unordered_map>

#include "music.h"

#include <SFML/Audio.hpp>

namespace music {
	
	sf::Music mainMusic;
	
	int currentMusic = -1;

	void switchTo(int musicId) {

		if(currentMusic == musicId) return;
		currentMusic = musicId;

		mainMusic.stop();
		if(currentMusic < 0) return;
		if(!mainMusic.openFromFile("mus/"+std::to_string(musicId)+".ogg")) {
			std::cerr << "Cannot load music file!" << std::endl;
		} else {
			mainMusic.setLoop(true);
			mainMusic.play();
		}

	}
	
	std::unordered_map<std::string, sf::Sound*> soundCache;

	void cleanup() {
		mainMusic.stop();
		
		// TODO iterate over sound cache here and delete buffers
	}
	
	void cacheSound(std::string soundName) {
		if(soundCache.find(soundName) == soundCache.end()) {
			sf::SoundBuffer* buffer = new sf::SoundBuffer;
			buffer->loadFromFile("snd/"+soundName+".wav");
			sf::Sound* sound = new sf::Sound(*buffer);
			soundCache[soundName] = sound;
		}
	}
	
	void playSound(std::string soundName, double pitch) {
		cacheSound(soundName);
		
		sf::Sound* sound = soundCache[soundName];
		sound->setPitch(pitch);
		sound->play();
	}
}
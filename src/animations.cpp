#include "core.h"
#include "animations.h"
#include "timekeeping.h"

namespace animations {
	void init(){
		// initialize all standard animations here
	}

	AnimationInstance::AnimationInstance(int x, int y, bool t, const std::shared_ptr<animations::animation>& _a) : offsetX(x), offsetY(y), atTile(t), a(_a) {
		sprite.setTexture(*a->texture);
		sprite.setPosition(x,y);
		start = timekeeping::currentTick();
	}

	sf::Sprite* AnimationInstance::update(){
		int now = timekeeping::currentTick();
		if((now - start)/a->speed >= a->frames) {
			if(!a->loop) return NULL;
			else start = now;
		}
		sf::IntRect ir = a->frameRects[(now - start)/a->speed];
		sprite.setTextureRect(ir);
		return &sprite; 
	}
}


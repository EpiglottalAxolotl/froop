
#include <SFML/Window.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
//#include "animations.h"
//#include "characters.h"
#include <list>
#include <memory>

#define SCREENWIDTH 500
#define SCREENHEIGHT 300

#define TOTALWIDTH (SCREENWIDTH+128)
#define TOTALHEIGHT (SCREENHEIGHT+64)

#define SECTIONPAD 8

namespace animations { struct animation; }

namespace display {

	void scheduleSetTile(int x, int y, int t);
	void loadBackground(unsigned char** imgIds, unsigned char** typeIds, int width, int height, int tileset);
	
	void drawWindow(sf::RenderWindow& window);
	void drawTitleScreen(sf::RenderWindow& window);
	
	void setGlobalScale(int);
	int getGlobalScale();
	void updateMouse(int,int);
	
	void clearView();
	void showInventory();
	void showGroundItems();
	void showCharacterStatus();
	bool isClearView();
	bool isStartMenu();
	
	void addAnimation(int, int, animations::animation*);
	void addAnimationAtTile(int, int, animations::animation*);
	void showDamageText(int, int, int);
	
	void center(int x, int y); // centers on the (x,y)th tile of the grid
	
	void showDialogue(const std::string&, const std::string&, int, int voiceid = -1);
	void showDialogueOptions(int, const std::shared_ptr<std::vector<std::string> >&);
	void skipDialogue();
	void hideDialogue();
	bool hasDialogue();
	
	void showSceneImage(int);
	
	void init();
	void cleanup();

	void updateBackground();
}

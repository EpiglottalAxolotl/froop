#ifndef PLAYERCHAR_H
#define PLAYERCHAR_H

#include <SFML/Graphics.hpp>

#include "core.h"

namespace playerchar {

	void init();

	int getX();
	int getY();
	
	void onArrowKey(direction);
	
	sf::Sprite& getSprite();
}

#endif
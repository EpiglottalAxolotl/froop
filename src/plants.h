#ifndef PLANTS
#define PLANTS

struct PlantType {
	int defaultGrowthRate; // how many stages this plant grows per night
	
	int defaultYield;
	int lightYield;
	int darkYield;
	
	int growthTime; // what stage of growth the plant stops growing at
	int stagesPerFrame; // how many consecutive growth stages each image is used for
	
	int id;
};

struct Plant {
	int plantId;         // 4-6 bits
	
	int growthStage;     // 4ish bits
	int harvestReady;    // 2-3 bits
	
	int harvestAccumulated;
};

#define NO_PLANT 0
#define PLANT_FROOP 1
#define PLANT_SPECIAL_FROOP 2
#define PLANT_SPECIAL_WATER_FROOP 3
#define PLANT_DRILL 4
#define PLANT_GROW1 5
#define PLANT_WATER1 6
#define PLANT_SUNFLOWER 7
#define PLANT_MOONFLOWER 8

#define TYPE_NORMAL 0
#define TYPE_CONCRETE 3
#define TYPE_POOL 4

namespace plantTypes {
	const PlantType get(int);
	void init();
}

#endif
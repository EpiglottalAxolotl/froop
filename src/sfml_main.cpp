
#include <thread>
#include <string>
#include <iostream>
#include <fstream>

#include "buttons.h"
#include "display.h"
#include "world.h"
#include "animations.h"
#include "cutscenes.h"
#include "music.h"
#include "timekeeping.h"
#include "items.h"
#include "plants.h"
#include "playerchar.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#ifdef NATIVE_THREADS
#include <thread>
#endif

using namespace std;

void resize(sf::RenderWindow& window, int w, int h) {
	window.setView(sf::View(sf::FloatRect(0,0,w,h)));
	
	int xScale = w / TOTALWIDTH;
	int yScale = h / TOTALHEIGHT;
	
	display::setGlobalScale(xScale > yScale ? yScale : xScale);
}


int main(){

	cout << "START" << endl;
	playerchar::init();
	plantTypes::init();
	items::init();
	cout << "Initialized Items" << endl;
	display::init();
	cout << "Initialized Display" << endl;
	animations::init();
	cout << "Initialized Animations" << endl;
	world::loadSave();
	cout << "Loaded Map" << endl;
	buttons::init();
	cout << "Initialized User Interface" << endl;
	cutscenes::init();

	sf::Image icon;
	icon.loadFromFile("img/icon.png");
	cout << "main: loaded icon" << endl;

	sf::RenderWindow window(sf::VideoMode(TOTALWIDTH, TOTALHEIGHT), "grid farm game");
	
	//TODO screensize * tilesize + menusize
	cout << "main: created window" << endl;
	window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
	cout << "main: set icon" << endl;

#ifdef NATIVE_THREAD
	std::thread clockThread(timekeeping::clock);
#else
	sf::Thread clockThread(&timekeeping::clock);
	clockThread.launch();
#endif

	//buttons::showTitleScreen();

	sf::Event event;
	while(buttons::gameOpen){
		
		while (window.pollEvent(event)) {
			const unordered_set<Button*>& visibleButtons = buttons::listButtons();
		
			switch(event.type) {
				
				case sf::Event::Resized: {
					resize(window, event.size.width, event.size.height);
					break;
				}
	
				case sf::Event::Closed: {
					// Trigger some sort of confirmation, actually. But that can be handled later.
					buttons::gameOpen = false;
					break;
				}

				case sf::Event::MouseMoved: {
					int scale = display::getGlobalScale();
					int x = event.mouseMove.x / scale;
					int y = event.mouseMove.y / scale;
				
					for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
						if((*itr)->rect.contains(x, y)){
							(*itr)->highlight();
						} else {
							(*itr)->reset();
						}
					}
					display::updateMouse(event.mouseMove.x, event.mouseMove.y);
					
					if (x < SCREENWIDTH && y < SCREENHEIGHT) {
						buttons::updateMousePosition(x, y);
					}
					
					break;
				}
				
				case sf::Event::MouseWheelScrolled: {
					buttons::onScroll(-(int)(event.mouseWheelScroll.delta));
					break;
				}
				
				case sf::Event::MouseButtonPressed: {
					for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
						if((*itr)->rect.contains(event.mouseButton.x / display::getGlobalScale(), event.mouseButton.y / display::getGlobalScale())){
							(*itr)->depress();
						} else {
							(*itr)->reset();
						}
					}
					break;
				}
				
				case sf::Event::MouseButtonReleased: {
					if(event.mouseButton.button == sf::Mouse::Button::Left){
						int mouseX = event.mouseButton.x / display::getGlobalScale();
						int mouseY = event.mouseButton.y / display::getGlobalScale();
						bool handledClick = false;
						Button* currentButton;
						for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
							currentButton = *itr;
							if(currentButton->rect.contains(mouseX, mouseY) && !currentButton->isDisabled()){
								currentButton->effect();
								currentButton->reset();
								currentButton->highlight();
								handledClick = true;
								break;
							}
						}
						if(!handledClick && display::hasDialogue()) {
							handledClick = true;
							if(cutscenes::isAdvanceable()) {
								cutscenes::advanceDialogue();
							} else {
								display::skipDialogue();
							}
						}
						
						if(!handledClick) {
							buttons::onTarget();
						}
					}
					break;
				}
				
				case sf::Event::KeyPressed: {
					switch(event.key.code){
						case sf::Keyboard::A:
						case sf::Keyboard::Left:  buttons::onArrowKey(direction::_left); break;
						case sf::Keyboard::D:
						case sf::Keyboard::Right: buttons::onArrowKey(direction::_right); break;
						case sf::Keyboard::W:
						case sf::Keyboard::Up:    buttons::onArrowKey(direction::_up); break;
						case sf::Keyboard::S:
						case sf::Keyboard::Down:  buttons::onArrowKey(direction::_down); break;
					}
				}
			}
		}
		
		if (display::isStartMenu()) {
			//display::drawTitleScreen(window);
		} else {
			display::drawWindow(window);
		}
		
	}
	
	timekeeping::stop();

#ifdef NATIVE_THREAD
	clockThread.join();
#else
	clockThread.wait();
#endif

	music::cleanup();
	
	window.close();

	return 0;
}

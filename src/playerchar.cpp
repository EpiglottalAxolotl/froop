#include "playerchar.h"
#include "core.h"

#include <iostream> //temporarily

namespace playerchar {

	int x;
	int y;
	direction dir;
	
	sf::Texture texture;
	sf::Sprite sprite;
	
	int getX() { return x; }
	int getY() { return y; }
	
	void init() {
		texture.loadFromFile("img/pc2.png");
		
		sprite.setTexture(texture);
		sprite.setTextureRect(sf::IntRect(0,0,24,30));
	}
	
	bool canMoveTo(int x, int y) {
		if (x < 0 || y < 0 || x >= GARDEN_SIZE || y >= GARDEN_SIZE) return false;
		return true; // check terrain type
	}
	
	void setDirection(direction _dir) {
		// TODO copy over mlq code for actual animations here, maybe? ...or maybe not
		dir = _dir;
		sprite.setTextureRect(sf::IntRect(24*dir, 0, 24, 30));
	}
	
	void onArrowKey(direction _dir) {
		int newX = x;
		int newY = y;
		switch(_dir){
			case _up: --newY; break;
			case _down: ++newY; break;
			case _left: --newX; break;
			case _right: ++newX; break;
		}
		
		setDirection(_dir);
		
		if(canMoveTo(newX, newY)) {
			// animations and stuff...
			
			x = newX;
			y = newY;
		}
	}
	
	sf::Sprite& getSprite() {
		return sprite;
	}

}
#ifndef __ITEMS__
#define __ITEMS__

#include <unordered_map> // TODO actually use this
#include <vector>
#include <SFML/Graphics.hpp>

// item categories
#define ICAT_default 0
#define ICAT_potion 1

#define ITEM_IMGSIZE 24

class StattedCharacter;

enum itemEffectType {
	plantCrop, defaultEffect, misc, harvest
};

typedef struct {
	itemEffectType effect;
	int* params;
	//int param1;
	//int param2;
} itemEffect;


class item {
	public:
		sf::Sprite* getSprite() const { return sprite; }
		const std::string getName() const { return name; }
		const std::string getFlavor() const { return flavor; }
		sf::Sprite* sprite;
		std::string name;
		std::string flavor;
		itemEffect useEffect;
    
		const int id;
		
		int category;
    
		item(int _id) : id(_id) {}
};

namespace items {
	void init();
	item* getItem(int);
	
	bool useItem(itemEffect, int, int);
	
	bool isPermanent(item* const);
}

namespace inventory {
	int count();
	item* getItem(int);
	bool hasItem(int);
	bool removeItemAtIndex(int);
	bool removeItem(int);
	bool removeItems(int, int);
	void takeItem(int);
	void takeItems(int, int);
	
	typename std::map<int,int>::const_iterator enumerateItems();
	typename std::map<int,int>::const_iterator itemsEnd();
}

#endif

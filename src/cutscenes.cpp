#include "cutscenes.h"
#include "display.h"
#include "buttons.h"
#include "music.h"
#include "items.h"

#include <vector>
#include <fstream>
#include <sstream>

#include <iostream>

//forward declaration
namespace world {
	bool hasGlobalFlag(int);
	void addGlobalFlag(int);
	void removeGlobalFlag(int);
	
	void putItem(int, int, item*);
}

namespace display {
	void center(int, int);
}

namespace cutscenes {
	
	std::vector<cutsceneEvent> roomCutscene;
	std::vector<cutsceneEvent> fileCutscene;
	int itr;
	int waitFor = 0;
	
	std::vector<std::string> minorCharacterNames;
	std::vector<std::string> majorCharacterNames;
	std::vector<int> talkspriteOffsets;
	
	bool hasDialogue;
	bool canAdvance = true;
	
	void init() {
		std::ifstream majCharFile("misc/majchar.asdf");
		std::string line;
		std::stringstream lineStream;
		int talkspriteOffset = 0;
		while(getline(majCharFile, line)){
			lineStream.str(line);
			lineStream.clear();
			
			talkspriteOffsets.push_back(talkspriteOffset);
			
			std::string temp;
			std::getline(lineStream, temp, '\t');
			majorCharacterNames.push_back(temp);
			
			std::getline(lineStream, temp, '\t');
			talkspriteOffset += std::stoi(temp);
		}
		
		std::ifstream minCharFile("misc/minchar.asdf");
		while(getline(minCharFile, line)) minorCharacterNames.push_back(line);
		
		hasDialogue = false;
	}
	
	void loadDialogue(const std::string& filename, std::vector<cutsceneEvent>& cutscene) {
		cutscene.clear();
		
		std::ifstream scriptFile(filename);
		if(!scriptFile.good()) {
			std::cout << "can't load file " << filename << std::endl;
			return;
		}
		
		std::string line;
		std::stringstream lineStream;
		
		while(getline(scriptFile, line)){
			lineStream.str(line);
			lineStream.clear();
			
			char eventType;
			lineStream.get(eventType);
			
			std::string temp;
			
			bool specialInit = false;
			int paramCount;
			
			cutsceneEvent event;
			event.type = eventType;
			
			switch(eventType) {
				case 'd': { // dialogue
					// need to specify tabs because << stops at any whitespace and dialogue lines will have spaces
					std::getline(lineStream, temp, '\t');
					int minChar = std::stoi(temp);
					std::getline(lineStream, temp, '\t');
					int majChar = std::stoi(temp);
					
					std::string text;
					std::getline(lineStream, text, '\t');
					
					dialogueLine line = {minChar, majChar, text};
					event.details = line;
					
					specialInit = true;
					break;
				}
				
				case 'N': { // narration
					
					std::string text;
					std::getline(lineStream, text, '\t');
					
					dialogueLine line = {-2, -1, text};
					event.details = line;
					
					specialInit = true;
					break;
				}
				
				case 'b': { // branch
					
					std::getline(lineStream, temp, '\t');
					int minChar = std::stoi(temp);
					std::getline(lineStream, temp, '\t');
					int majChar = std::stoi(temp);
					
					std::string text;
					std::getline(lineStream, text, '\t');
					
					dialogueLine line = {minChar, majChar, text};
					
					std::getline(lineStream, temp, '\t');
					int count = std::stoi(temp);
					
					std::shared_ptr<std::vector<int>> indices { new std::vector<int>(count, 0) };
					std::shared_ptr<std::vector<std::string>> textOptions { new std::vector<std::string>(count, std::string()) };
					
					for(int i=0; i<count; ++i) {
						std::getline(lineStream, temp, '\t');
						(*indices)[i] = std::stoi(temp);
						std::getline(lineStream, (*textOptions)[i], '\t');
					}
					
					dialogueBranch branch = {count, line, indices, textOptions};
					event.details = branch;
					
					specialInit = true;
					break;
				}
				
				case 'n':   // walk (npc)
				case 'w': { // walk
					
					std::getline(lineStream, temp, '\t');
					int charId = std::stoi(temp);
					std::getline(lineStream, temp, '\t');
					int steps = std::stoi(temp);
					
					std::shared_ptr<std::vector<int> > params { new std::vector<int>(steps*2 + 2, 0) };
					(*params)[0] = charId;
					(*params)[1] = steps;
					
					for(int i=0; i<steps*2; ++i){
						std::getline(lineStream, temp, '\t');
						(*params)[i+2] = std::stoi(temp);
					}
					
					event.details = params;
					specialInit = true;
					break;
				}
				
				case 'e': // End cutscene
					paramCount = 0; break;
				case 'f': // load cutscene File and continue
				case 'j': // unconditional Jump
				case 'p': // Parallel: execute next n instructions, then pause until all complete
				case 's': // play Sound
				case 'm': // change Music
				case 'i': // display Image
				case 'C': // Checkpoint - save or heal
				case 'r': // Remove item from inventory
				case 'A': // Add room flag
				case 'a': // Add global flag
				case 'R': // Remove global flag
					paramCount = 1; break;
				case 't': // activate Trigger on map
				case 'F': // branch on room Flag
				case 'G': // branch on Global flag
				case 'c': // center Camera on xy coordinates
				case 'I': // conditional on Item
				case '@': // party member joins
				case 'g': // Give item
					paramCount = 2; break;
				
				// a parallel command can be followed by at most one dialogue (d) line, and any number of other non-control-flow-affecting lines.
				// branch (b), end (e), file (f), jump (j), and parallel (p) are considered control-flow-affecting and are thus not elligible.
				// trigger (t) is elligible, but also instantaneous, so it doesn't make much sense
				// mostly it's going to be moving characters (w) and playing sounds (s) that gets parallelized
			}
			
			if(!specialInit) { // default initialization
				std::shared_ptr<std::vector<int>> params { new std::vector<int>(paramCount, 0) };
				for(int i=0; i<paramCount; ++i) {
					std::getline(lineStream, temp, '\t');
					(*params)[i] = std::stoi(temp);
				}
				event.details = params;
			}
			
			cutscene.push_back(event);
		}
	}
	
	void loadRoomDialogue(int id) {
		loadDialogue("map/script/"+std::to_string(id), roomCutscene);
	}
	
	void loadDialogueFile(int id) {
		loadDialogue("script/"+std::to_string(id), fileCutscene);
	}
		
	void startDialogueFromBranch(int index) {
		cutsceneEvent branchEvent = (fileCutscene.size() > 0) ? fileCutscene[itr] : roomCutscene[itr];
		itr = (*branchEvent.getBranch().indices)[index];
		advanceDialogue();
	}
	
	void startDialogue(int startIndex) {
		itr = startIndex;
		hasDialogue = true;
		buttons::setInteractive(false);
		advanceDialogue();
	}
	
	void setAdvanceable() {
		canAdvance = true;
	}
	
	bool isAdvanceable() {
		return canAdvance;
	}
	
	void showDialogueLine(const dialogueLine& line) {
		std::string name = "";
		int talksprite = -1;
		if(line.majorCharacter >= 0) {
			talksprite = talkspriteOffsets[line.majorCharacter] + line.minorCharacter;
			name = majorCharacterNames[line.majorCharacter];
		} else if(line.minorCharacter >= 0){
			name = minorCharacterNames[line.minorCharacter];
		}
		display::showDialogue(line.text, name, talksprite, line.majorCharacter);
	}
	
	void advanceDialogue(const std::vector<cutsceneEvent>& cutscene) {
		if(!canAdvance) return;
		canAdvance = true;
		if(waitFor) {
			--waitFor;
			return;
		}
        
        const cutsceneEvent& ce = cutscene[itr];
        
		if(!hasDialogue || itr == cutscene.size() || ce.type == 'e') {
			hasDialogue = false;
			display::hideDialogue();
			if(fileCutscene.size() > 0) fileCutscene.clear();
			buttons::setInteractive(true);
		}
		else if (ce.type == 'N' || ce.type == 'd') {
	
			canAdvance = false;
			showDialogueLine(ce.getLine());

			++itr;
			return;

		} else if (ce.type == 'b') {
			const dialogueBranch& branch = ce.getBranch();
			showDialogueLine(branch.prompt);
			
			buttons::showDialogueOptionButtons(branch.count);
			
			display::showDialogueOptions(branch.count, branch.textOptions);
			return;
        } else {
            
            const std::vector<int>& p = *ce.getParams();
            
            switch(ce.type) {
				
				// Control flow
				
				case 'f': {
					loadDialogueFile(p[0]);
					startDialogue(0);
					return;
				}
				case 'j': {
					startDialogue(p[0]);
					return;
				}
				case 'p': {
					int parallels = p[0];
					for(int i=0; i<parallels; ++i) advanceDialogue();
					waitFor = parallels;
					return;
				}
				case 'G': {
					if(world::hasGlobalFlag(p[0])) {
						startDialogue(p[1]);
						return;
					} else break;
				}
				case 'I': {
					if(inventory::hasItem(p[0])) {
						startDialogue(p[1]);
						return;
					} else break;
				}
				
				// Other
				
				case 'm': {
					music::switchTo(p[0]);
					break;
				}
				case 'c': {
					display::center(p[0],p[1]);
					break;
				}
				case 'i': {
					display::showSceneImage(p[0]);
					break;
				}
				case 'r': {
					inventory::removeItem(p[0]);
					break;
				}
				case 'g': {
					inventory::takeItem(p[0]);
					break;
				}
				case 'a': {
					world::addGlobalFlag(p[0]);
					break;
				}
				case 'R': {
					world::removeGlobalFlag(p[0]);
					break;
				}
				
			}
			
			++itr;
			advanceDialogue(cutscene);
		}
	}
	
	void advanceDialogue() {
		if(fileCutscene.size() > 0) advanceDialogue(fileCutscene);
		else advanceDialogue(roomCutscene);
	}
	
}

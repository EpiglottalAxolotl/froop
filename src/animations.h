#ifndef __ANIMATIONS__
#define __ANIMATIONS__

#include <SFML/Graphics.hpp>
#include <memory>

#include "core.h"

namespace animations {
	struct animation {
		int frames;
		int speed;
		bool loop;
		sf::IntRect* frameRects;
		sf::Texture* texture;
	};
	
	void init();
	
	class AnimationInstance {
		public:
			AnimationInstance(int, int, bool, const std::shared_ptr<animations::animation>&);
			
			const int offsetX;
			const int offsetY;
			const bool atTile;
			
			sf::Sprite* update();
		
		private:
			const std::shared_ptr<animations::animation> a;
			int start;
			int frame;
			sf::Sprite sprite;
	};
}

#endif

#include "items.h"
#include "display.h"
#include "world.h"

#include <fstream>
#include <sstream>

#include <iostream>
using namespace std;

namespace items {
	
	std::vector<item*> allItems;
	item* getItem(int id){
		return allItems[id];
	}

	sf::Texture* itemsTexture;

	itemEffect stringToEffect(std::string s){
		stringstream ss;
		ss.str(s);
		itemEffect output;
		char type;
		ss >> type;
		int params;
		switch(type){
			case '-': output.effect = itemEffectType::defaultEffect; output.params = 0; return output;
			case 'p': params = 1; output.effect = itemEffectType::plantCrop; break;
			case 'h': params=0; output.effect=itemEffectType::harvest;break;
			default: std::cout << "String [" << s << "] results in unknown type " << type << std::endl;
		}
		
		output.params = new int[params];
		for(int i=0; i<params;++i) ss >> output.params[i];
		return output;
	}

	void init(){
		itemsTexture = new sf::Texture;
		itemsTexture->loadFromFile("img/all_items.png");
		
		int tilesetWidth = itemsTexture->getSize().x / ITEM_IMGSIZE;
		//int tilesetHeight = itemsTexture->getSize().y / ITEM_IMGSIZE;
		
		//Adhoc Serialized Data Format
		ifstream itemFile("misc/items.asdf");
		string line;
		stringstream lineStream;
		int id = 0;
		while(getline(itemFile, line)){
			lineStream.str(line);
			lineStream.clear();
			// Line format is {name}\t{flavor}\t{category}\t{useEffect}
			
			item* i = new item(id);
			
			getline(lineStream, i->name, '\t');
			getline(lineStream, i->flavor, '\t');
			string temp;
			
			getline(lineStream, temp, '\t');
			i->category = stoi(temp);
			
			getline(lineStream, temp, '\t');
			i->useEffect = stringToEffect(temp);
			
			i->sprite = new sf::Sprite;
			i->sprite->setTexture(*itemsTexture);
			i->sprite->setTextureRect(sf::IntRect(id%tilesetWidth * ITEM_IMGSIZE, id/tilesetWidth * ITEM_IMGSIZE, ITEM_IMGSIZE, ITEM_IMGSIZE));
			
			allItems.push_back(i);
			++id;
		}
		inventory::takeItem(0);
		inventory::takeItems(1, 6);
	}

	bool useItem(itemEffect ie, int x, int y) {
		// Returns true if the item is consumed
		
		switch(ie.effect){
			case plantCrop:
				world::plantCrop(ie.params[0], x, y);
				break;
			case harvest:
				world::harvestCrop(x, y);
				break;
			case misc:
				std::cout << "item is other" << std::endl;
				switch(ie.params[0]){
					// ...
				}
				break;
			default:
				std::cout << "item is...? " << ie.effect << std::endl;
		}
		return false;
	}
}

	
namespace inventory {

	std::map<int, int> currentItems;

	std::map<int, int>::const_iterator enumerateItems() {
		return currentItems.cbegin();
	}
	
	std::map<int, int>::const_iterator itemsEnd() {
		return currentItems.cend();
	}

	bool hasItem(int id) {
		auto itr = currentItems.find(id);
		return !(itr == currentItems.end()) && itr->second > 0;
	}
	
	void takeItem(int id) {
		takeItems(id, 1);
	}
	
	void takeItems(int id, int count) {
		currentItems[id] += count; // operator[] initializes to default value if not present
	}
	
	bool removeItem(int id) {
		return removeItems(id, 1);
	}
	
	bool removeItems(int id, int count) {
		auto itr = currentItems.find(id);
		
		auto end = currentItems.end();
		
		if (itr == currentItems.end() || itr->second < count) return false;
		
		currentItems[id] -= count;

		if (itr->second == 0) currentItems.erase(itr);

		return true;
	}
	
	bool removeItemAtIndex(int index) {
		auto itr = currentItems.begin();
		for(int i=0; i<index; ++i) {
			++itr;
			if(itr == currentItems.end()) return false;
		}
		
		itr->second -= 1;
		if (itr->second == 0) currentItems.erase(itr);
		
		return true;
	}
	
	item* getItem(int index) {
		auto itr = currentItems.begin();
		for(int i=0; i<index; ++i) ++itr;
		return items::getItem(itr->first);
	}
	
	int count() {
		return currentItems.size();
	}
}
